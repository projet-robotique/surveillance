#ifndef TOF_SENSOR_H_
#define TOF_SENSOR_H_

// Start the TOF thread
void tof_sensor_start(void);
// Stop the TOF thread
void tof_sensor_stop(void);

#endif /* TOF_SENSOR_H_ */
