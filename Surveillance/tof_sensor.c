#include <main.h>
#include <chprintf.h>
#include <arm_math.h>
#include "i2c_bus.h"
#include <motors.h>
#include <sensors/VL53L0X/VL53L0X.h>
#include "sensors/VL53L0X/Api/core/inc/vl53l0x_api.h"
#include "leds.h"

#include <pi_regulator.h>

// Constants
#define NB_TURN_INIT 			2 // Number of turn that the robot should make to initialize the tab
#define DIST_THRESHOLD			20 // mm
#define NB_POINT_PER_ROT		132 // Theoretical value for the number of measure per turn
#define CORR_NB_POINT_PER_ROT	129 // Practical value for the number of measure per turn
#define INFINITE_THRESH			8000 // mm
#define NB_VAL_TEST				2 // Number of values that are tested before and after the actual
								  // value in the algorithm of object detection

static thread_t *distThd;
static VL53L0X_Dev_t device;

// Boolean used for the threads
static bool TOF_configured = false, pi_start = false;

// Variable used to initialize the distance memory
static bool dist_tab_init = false;
static uint16_t nb_dist_init = 0;

// Variable used in the TOF measure and object detection algorithm
static int32_t nb_step, last_nb_step = 0;
static uint16_t dist_mm = 0, dist_memory[CORR_NB_POINT_PER_ROT], buf_dist[NB_VAL_TEST+1];
static uint8_t infinite_warning = 0;

void object_detection(uint16_t distance, int32_t position){

	uint8_t obj_detecting = 0, infinite_detec = 0;
	int32_t position_to_test = 0;
	static bool obj_detected = false;

	// Loop to test the n values before and after the actual measure
	for(int8_t i = -NB_VAL_TEST; i <= NB_VAL_TEST; i++){

		position_to_test = (position+CORR_NB_POINT_PER_ROT+i)%CORR_NB_POINT_PER_ROT;

		if(distance < dist_memory[position_to_test]-DIST_THRESHOLD){

			// If the distance in memory is set to infinite (8190 or 8191),
			// we increment a variable that will be used after
			if(dist_memory[position_to_test] > INFINITE_THRESH){
				infinite_detec++;
			}

			obj_detecting++;
		}
	}

	// If all the distance memory are not infinite, reset the infinite warning
	if(infinite_detec != (NB_VAL_TEST*2+1)){
		infinite_warning = 0;
	}

	// Checking the different conditions of the object detection
	if(!obj_detected && obj_detecting == NB_VAL_TEST*2+1
			&& (infinite_detec == 0 || infinite_warning > 1)){

		obj_detected = true;

		// Detection procedure
		set_front_led(1);

		pi_start = false;
		pi_regulator_stop();

		chThdSleepMilliseconds(5000);
		set_front_led(0);

		pi_regulator_start();
		pi_start = true;

		//We need to wait that the PI regulator thread restart
		chThdSleepMilliseconds(25);

		//End of detection procedure
	}

	// Detection of the end of the object
	if(obj_detected && obj_detecting != NB_VAL_TEST*2+1){
		obj_detected = false;
	}

	// Checking if all the distance memory tested are infinite
	if(infinite_detec == NB_VAL_TEST*2+1){
		infinite_warning++;
	}

	// Replace the distance memory of the data that will not be used in the next loop
	// with the data that have been measure during this turn
	dist_memory[(position+CORR_NB_POINT_PER_ROT-(NB_VAL_TEST))%CORR_NB_POINT_PER_ROT]
				= buf_dist[(position+1)%(NB_VAL_TEST+1)];
}

void tof_measure(void){

	// Reading and computing the measure that have been taken the last loop
	VL53L0X_getLastMeasure(&device);
	dist_mm = device.Data.LastRangeMeasure.RangeMilliMeter;
	int32_t position = (int32_t)(last_nb_step*NB_POINT_PER_ROT/NB_STEP_PER_ROBOT_ROT);

	// Beginning the measurement that will be read and compute next loop
	VL53L0X_startMeasure(&device, VL53L0X_DEVICEMODE_SINGLE_RANGING);

	// Initialization of the distance memory with a definite number of turn
	if(!dist_tab_init){
		set_body_led(1);
		dist_memory[position%CORR_NB_POINT_PER_ROT] = dist_mm;
		nb_dist_init++;
	}
	if(!dist_tab_init && nb_dist_init == NB_POINT_PER_ROT*NB_TURN_INIT){
		set_body_led(0);
		dist_tab_init = true;
	}

	// After the initialization begin the treatment
	if(dist_tab_init)
		object_detection(dist_mm, position);

	// Storing the distance to permit the replacement of the values with n delays step
	buf_dist[position%(NB_VAL_TEST+1)]=dist_mm;
	last_nb_step = nb_step;

	return;
}

static THD_WORKING_AREA(waTOFThd, 512);
static THD_FUNCTION(TOFThd, arg) {

	chRegSetThreadName("TOF Thd");
	VL53L0X_Error status = VL53L0X_ERROR_NONE;

	systime_t time;
	time = chVTGetSystemTime();

	(void)arg;

	device.I2cDevAddr = VL53L0X_ADDR;

	status = VL53L0X_init(&device);

	if(status == VL53L0X_ERROR_NONE){
		VL53L0X_configAccuracy(&device, VL53L0X_HIGH_SPEED);
	}
	if(status == VL53L0X_ERROR_NONE){
		VL53L0X_startMeasure(&device, VL53L0X_DEVICEMODE_SINGLE_RANGING);
	}
	if(status == VL53L0X_ERROR_NONE){
		TOF_configured = true;
	}

	bool measure_done = false;

    while (chThdShouldTerminateX() == false) {
    	// As the rotation speed of the robot is not constant (PI regulator)
    	// the measure are not done periodically.
    	// So we have to wait for the measure to be done before doing any calculation.

    	// Loop to wait for the measure to be done at the right step
    	while(measure_done == false){

    		time = chVTGetSystemTime();
    		nb_step = right_motor_get_pos();

    		// Checking if it is the right step to do the measure
    		if(TOF_configured && !(nb_step%(int)(NB_STEP_PER_ROBOT_ROT/NB_POINT_PER_ROT))
    				&& nb_step != last_nb_step){
    			tof_measure();
    			measure_done = true;
    		}
    	}

    	measure_done = false;

    	// TIME_PER_ROT/NB_POINT_PER_ROT = 5/132 ~= 37 ms
    	// So we decide to wait a little less than 37 ms to be sure to not miss a measuring step
    	chThdSleepUntilWindowed(time, time + MS2ST(30));
    }
}

void tof_sensor_start(void){

	if(TOF_configured) {
		return;
	}

	i2c_start();

	distThd = chThdCreateStatic(waTOFThd,
					 sizeof(waTOFThd),
					 NORMALPRIO + 10,
					 TOFThd,
					 NULL);

	//Starting the robot rotation
	if(!pi_start){
		pi_regulator_start();
		pi_start = true;
	}
}

void tof_sensor_stop(void) {

	chThdTerminate(distThd);
    chThdWait(distThd);
    distThd = NULL;
    TOF_configured = false;

    //Stopping the robot rotation
    if(pi_start){
		pi_regulator_stop();
		pi_start = false;
	}
}

