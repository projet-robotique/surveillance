#include "ch.h"
#include "hal.h"
#include <math.h>
#include <usbcfg.h>
#include <chprintf.h>

#include <main.h>
#include <motors.h>
#include <sensors/imu.h>

#include <pi_regulator.h>

// Constants
#define Z_AXIS 			2
#define CIRC_BUF_SIZE	5
#define INT16_MAX_NB	32768
#define IMU_RANGE_DEG	250
#define IMU_RANGE_RAD 	(IMU_RANGE_DEG / 180 * M_PI)
#define INT_TO_RAD_S	(IMU_RANGE_RAD / INT16_MAX_NB)

static thread_t *piThd;

// Table to stock the last X values of speed used in the moving average filter
static int16_t circ_buffer[CIRC_BUF_SIZE];
// Variable to stock the gyroscope offset on z axis
static int16_t z_offset;

// Speed PI regulator using a moving average filter
int16_t pi_regulator(int16_t act_speed, float goal){

	float error = 0;
	int32_t mean_speed = 0;
	static float speed = 0;
	static float sum_error = 0;
    static uint8_t buf_pos = 0;

    circ_buffer[buf_pos] = act_speed;
    buf_pos = (buf_pos+1)%CIRC_BUF_SIZE;

    // Moving average filter
    for(uint8_t i=0; i<CIRC_BUF_SIZE; ++i){
    	mean_speed += circ_buffer[i];
    }

	error = goal - (((float)mean_speed)/CIRC_BUF_SIZE - z_offset)*INT_TO_RAD_S;

	// Disables the PI regulator if the error is to small
	if(fabs(error) < ERROR_THRESHOLD){
		return speed;
	}

	sum_error += error;

	// We set a maximum and a minimum for the sum to avoid an uncontrolled growth
	if(sum_error > MAX_SUM_ERROR){
		sum_error = MAX_SUM_ERROR;
	}else if(sum_error < -MAX_SUM_ERROR){
		sum_error = -MAX_SUM_ERROR;
	}

	speed += KP * error + KI * sum_error;

    return (int16_t)speed;
}

static THD_WORKING_AREA(waPiRegulator, 256);
static THD_FUNCTION(PiRegulator, arg) {

    chRegSetThreadName(__FUNCTION__);
    (void)arg;

    systime_t time;

    int16_t speed = 0;

    while(chThdShouldTerminateX() == false){
        time = chVTGetSystemTime();
        
        // Computes the speed to give to the motors
        speed = pi_regulator(get_gyro(Z_AXIS), GOAL_SPEED);

        right_motor_set_speed(speed);
        left_motor_set_speed(-speed);

        // 100Hz
        chThdSleepUntilWindowed(time, time + MS2ST(10));
    }
}

void pi_regulator_start(void){

	// Initialization of the table for the moving average filter
	for(uint8_t i = 0; i < CIRC_BUF_SIZE; i++){
		circ_buffer[i] = 0;
	}

	z_offset = get_gyro_offset(Z_AXIS);

	piThd = chThdCreateStatic(waPiRegulator,
							sizeof(waPiRegulator),
							NORMALPRIO,
							PiRegulator,
							NULL);

}

void pi_regulator_stop(void) {

    chThdTerminate(piThd);
    chThdWait(piThd);
    piThd = NULL;

    // Stopping the motors
    right_motor_set_speed(0);
	left_motor_set_speed(0);
}
