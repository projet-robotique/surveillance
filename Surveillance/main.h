#ifndef MAIN_H
#define MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

#include "camera/dcmi_camera.h"
#include "msgbus/messagebus.h"
#include "parameter/parameter.h"

/** Robot wide IPC bus. */
extern messagebus_t bus;

extern parameter_namespace_t parameter_root;

#ifdef __cplusplus
}
#endif

#endif

// Constants
#define WHEEL_SPAN 				50 // mm
#define WHEEL_CIRC 				130// mm
#define ROBOT_CIRC 				(M_PI*WHEEL_SPAN) // mm
#define NB_STEP_PER_ROT 		1100
#define KP						20.0f
#define KI 						1.0f	// must not be zero
#define MAX_SUM_ERROR 			(MOTOR_SPEED_LIMIT/KI)
#define TIME_PER_ROT			5.0f // s
#define CORR_FACTOR_FOR_ROT		0.35f // rad/s (to adjust the real rotation speed with the theoretical one)
#define GOAL_SPEED 				(M_PI*2.0f/TIME_PER_ROT-CORR_FACTOR_FOR_ROT) // rad/s
#define ERROR_THRESHOLD 		(GOAL_SPEED*0.05) // rad/s
#define NB_STEP_PER_ROBOT_ROT	(ROBOT_CIRC/WHEEL_CIRC*NB_STEP_PER_ROT)
