#ifndef PI_REGULATOR_H
#define PI_REGULATOR_H

// Start the PI regulator thread
void pi_regulator_start(void);
// Stop the PI regulator thread
void pi_regulator_stop(void);

#endif /* PI_REGULATOR_H */
